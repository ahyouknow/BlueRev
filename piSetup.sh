#!/bin/bash

if [ "$EUID" -ne 0 ]
then
    echo "Run as with sudo"
    exit
fi

if [ "$#" -ne 3 ]
then
    echo "piSetup.sh IP_ADDRESS [X]COORDINATE [Y]COORDINATE"
    echo "example: piSetup.sh 127.0.0.1 250 250"
    exit
fi
apt-get update
apt-get -y install python-pip python-dev ipython
apt-get -y install bluetooth libbluetooth-dev
pip install pybluez
pip3 install pybluez
home="/home/pi"
wget -P $home https://raw.githubusercontent.com/ahyouknow/BlueRev/master/bluePi.py
sed -i '/exit 0/d' /etc/rc.local
echo "
python3 $home/bluePi.py $1 $2 $3
exit 0
" >> /etc/rc.local
echo "Successfully installed bluePi.py and should run on boot"
exit 0 
