#!/usr/bin/python3
import socket
from tkinter import *
import threading
import time
import itertools

def main():
    master = Tk()
    w = Canvas(master, width=1600, height=900)
    w.pack()
    w.configure(background="WHITE")
    img = PhotoImage(file="finalthing.png")
    w.create_image(0,0, anchor=NW, image=img)
    threading.Thread(target=serverbtw, args=(w, )).start()
    mainloop()

def serverbtw(w):
    event = threading.Event()
    threading.Thread(target=mainthread, args=(event, w)).start()
    while True:
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.bind(('', 6969))
            s.listen(0)
            while True:
                conn, addr = s.accept()
                threading.Thread(target=bluepi, args=(w, conn, event)).start()
        except:
            pass

def bluepi(w, conn, event):
    global devicesdict
    coords = conn.recv(1024).decode()
    coords = tuple(map(int, coords.split(' ')))
    devicesdict[coords] = ()
    pisize = (10, 5)
    pi = w.create_rectangle(coords[0]-pisize[0], coords[1]-pisize[1], coords[0]+pisize[0], coords[1]+pisize[1], fill="BLUE", outline="BLACK")
    lastdevices = ()
    while True:
        event.clear()
        data = conn.recv(1024).decode()
        if not data:
            conn.close()
            w.delete(pi)
            devicesdict.pop(coords)
            event.set()
            event.clear()
            return
        if data == 'null':
            devicesdict[coords] = ()
            lastdevices = ()
            event.set()
        else:
            devices = tuple(data.split(' '))
            if devices != lastdevices:
                devicesdict[coords] = devices
                lastdevices = devices
                event.set()

def mainthread(event, w):
    global devicesdict
    coordsls = []
    devicesls = []
    picturels = []
    oldcoords = []
    olddevices = []
    oldpicture = []
    randompictures = []
    radius = 10
    while True:
        event.wait()
        time.sleep(1)
        localdict = devicesdict
        for x in randompictures:
            w.delete(x)
        randompictures = []
        for pies in localdict.keys():
            devices, coords = dictCheck(pies, localdict, tuple(devicesls))
            for device, coord in zip(devices, coords):
                macAddr = device.split(':')
                color = str('#'+macAddr[2]+macAddr[4]+macAddr[5])
                if device in devicesls:
                    pass
                elif device in olddevices:
                    index = olddevices.index(device)
                    devicesls.append(device)
                    coordsls.append(coord)
                    if coord != oldcoords[index]:
                        w.delete(oldpicture[index])
                        picturels.append(w.create_oval(coord[0] - radius, coord[1] - radius, coord[0] + radius, coord[1] + radius, fill=color))
                    else:
                        picturels.append(oldpicture[index])
                else:
                    devicesls.append(device)
                    coordsls.append(coord)
                    picturels.append(w.create_oval(coord[0] - radius, coord[1] - radius, coord[0] + radius, coord[1] + radius, fill=color))
        lostdevices = list(set(olddevices) - set(devicesls))
        for device in lostdevices:
            index = olddevices.index(device)
            w.delete(oldpicture[index])
        for coords in set(coordsls):
            coordcount = coordsls.count(coords)
            if coordcount > 1:
                looplist = []
                for x in coordsls:
                    looplist.append(x)
                newcoords = [(-20,-20), (0,-20), (20,-20), (-20,0), (0,0), (20,0), (-20,20), (0,20), (20, 20)]
                for x, newcoord in zip(range(len(newcoords)), newcoords):
                    if x == coordcount+1:
                        break
                    index = looplist.index(coords)+x
                    circle = picturels[index]
                    device = devicesls[index]
                    macAddr = device.split(':')
                    color = str('#'+macAddr[2]+macAddr[4]+macAddr[5])
                    w.delete(circle)
                    picturels.insert(index, w.create_oval(newcoord[0]+coords[0] - radius, newcoords[1]coords[1] - radius, newcoords[0]+coords[0] + radius, newcoords[1]+coords[1] + radius, fill=color))
                    looplist.remove(coords)
        olddevices = devicesls
        oldcoords = coordsls
        oldpicture = picturels
        devicesls = []
        coordsls = []
        picturels = []
                
def dictCheck(coords, localdict, alldevicesls):
    coordsls = []
    devicesls = []
    locallist = localdict[coords]
    for device in locallist:
        triangulate = []
        for x in localdict.keys():
            if device in localdict[x]:
                triangulate.append(x)
        if device in alldevicesls:
            pass
        elif len(triangulate) > 1:
            midpoint = triangulation(triangulate)
            devicesls.append(device)
            coordsls.append(midpoint)
        else:
            devicesls.append(device)
            coordsls.append(coords)
    return devicesls, coordsls

def triangulation(coordslist):
    Xaverage = 0
    Yaverage = 0
    for coords in coordslist:
        Xaverage+=coords[0]
        Yaverage+=coords[1]
    Xaverage = Xaverage/len(coordslist)
    Yaverage = Yaverage/len(coordslist)
    return tuple((Xaverage, Yaverage))
    

if __name__ == '__main__':
    global devicesdict
    devicesdict = {}
    main()
