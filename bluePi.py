#!/usr/bin/python3
import socket
import bluetooth
import sys
import time 

def main():
    if len(sys.argv) != 4:
        print('bluePi.py $IP_ADDRESS $COORDINATE(X) $COORDINATE(Y)')
        print('example input: bluePi.py 127.0.0.1 850 400')
        exit()
    server = sys.argv[1]
    cords = ' '.join(sys.argv[2:4])
    count = 0
    while True:
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((server, 6969))
            s.sendall(bytes(cords, 'UTF-8'))
            print('connected')
            while int(time.strftime("%S"))%10 != 0:
                pass
            while True:
                devices = bluetooth.discover_devices(duration=10, flush_cache=False, lookup_names=False)
                print(devices)
                if len(devices) > 0:
                    s.sendall(bytes(str(' '.join(devices)), 'UTF-8'))
                else:
                    s.sendall(bytes(str('null'), 'UTF-8'))
        except KeyboardInterrupt:
            exit()
        except Exception as x:
            sys.stdout.write("\033[K")
            print('failed to connect attempting to reconnect'+('.'*round(count)), end="\r")
            count=(count+0.0001)%3
            
if __name__ == '__main__':
    main()
